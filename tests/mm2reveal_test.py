from lxml import etree
from lxml.etree import XML, tostring

import pytest

import mm2reveal
import mm2reveal.main

INPUT_FILE = 'example.mm'


@pytest.fixture(scope='module')
def input_file_name():
    import os

    return os.path.join(os.path.dirname(__file__), INPUT_FILE)


@pytest.fixture(scope='module')
def example_mm(input_file_name):
    with open(input_file_name) as input_file:
        tree = etree.parse(input_file)
        root = tree.getroot()
    return mm2reveal.main.parse_tree(root)


def test_fixws():
    sample = "<p>           Verbatim         Text       </p>"
    t = mm2reveal.main.fix_ws(sample)
    assert "<p>Verbatim Text</p>" == t


def test_text1():
    sample = XML("""
<bla><node CREATED="1286972364889" ID="ID_1023767509" MODIFIED="1286972368829" TEXT="3.Punkt"/><node
CREATED="1286955627384" HGAP="21" ID="ID_634365668" MODIFIED="1286981982056" VSHIFT="7"><richcontent
TYPE="NODE"><html><head></head><body><p>Verbatim Text</p></body></html></richcontent><attribute
NAME="beginEnd"
VALUE="verbatim"/></node></bla>""")
    t = mm2reveal.main.text(sample[1])
    assert "<p>Verbatim Text </p>" == t


def test_title(example_mm):
    assert example_mm['title'] == 'Titel der Präsentation'


if __name__ == '__main__':
    pytest.main()
