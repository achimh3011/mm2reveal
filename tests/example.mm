<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1285312101269" ID="ID_311489571" MODIFIED="1285312180648" TEXT="Titel der Pr&#xe4;sentation">
<node CREATED="1286977969470" ID="ID_261800896" MODIFIED="1287042443847" POSITION="right" TEXT="properties">
<icon BUILTIN="yes"/>
<node CREATED="1286977986920" ID="ID_1449983193" MODIFIED="1287038700647" TEXT="date=1.1.2010"/>
<node CREATED="1287038503845" ID="ID_1997055788" MODIFIED="1287038698816" TEXT="subtitle=Subtitel"/>
<node CREATED="1287038694995" ID="ID_1131423064" MODIFIED="1287038696710" TEXT="author=Robert Himmelmann"/>
<node CREATED="1287038813542" ID="ID_1216421530" MODIFIED="1287038825034" TEXT="place=One of Our Clients"/>
<node CREATED="1287039519414" ID="ID_918061947" MODIFIED="1287043638277" TEXT="usenavigation=false">
<node CREATED="1287065924900" ID="ID_880439411" MODIFIED="1287065949320" TEXT="hides or shows the navigation icons"/>
</node>
<node CREATED="1287065877587" ID="ID_139599624" MODIFIED="1287065901519" TEXT="autoquote=true">
<node CREATED="1287065902507" ID="ID_768303598" MODIFIED="1290340354443">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Automatically quote ampersands and underscores.
    </p>
    <p>
      Use this if you are not going to use math mode and want
    </p>
    <p>
      to avoid weird latex error messages
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1287043662842" ID="ID_1731701427" MODIFIED="1287043669063" POSITION="right" TEXT="preamble">
<icon BUILTIN="yes"/>
<node CREATED="1287044025270" ID="ID_451058526" MODIFIED="1287044037298" TEXT="\usepackage{tikz}"/>
</node>
<node CREATED="1285312194058" ID="ID_1410627575" MODIFIED="1286955894594" POSITION="right" TEXT="Irgendwas">
<node CREATED="1285312204884" ID="ID_871838064" MODIFIED="1287044219777">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Introduction
    </p>
  </body>
</html></richcontent>
<node CREATED="1285312216679" ID="ID_1900676421" MODIFIED="1285312227398" TEXT="TOC">
<node CREATED="1285312231588" ID="ID_697377686" MODIFIED="1285312236985" TEXT="1.Punkt">
<node CREATED="1285312237652" ID="ID_1363339684" MODIFIED="1285312241689" TEXT="1. Unterpunkt"/>
</node>
<node CREATED="1285312243757" ID="ID_1415533550" MODIFIED="1285312247644" TEXT="2. Punkt."/>
<node CREATED="1286972364889" ID="ID_1023767509" MODIFIED="1286972368829" TEXT="3.Punkt"/>
</node>
</node>
<node CREATED="1286955627384" HGAP="21" ID="ID_634365668" MODIFIED="1286981982056" VSHIFT="7">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Verbatim
    </p>
  </body>
</html></richcontent>
<node CREATED="1286978391885" ID="ID_739386094" MODIFIED="1286981991503" TEXT="Verbatim">
<attribute NAME="attributes" VALUE="fragile"/>
<node CREATED="1286978394813" ID="ID_43136974" MODIFIED="1286978445283" TEXT="vielQuellCode">
<attribute NAME="beginEnd" VALUE="verbatim"/>
</node>
<node CREATED="1286979600589" ID="ID_951506028" MODIFIED="1286979645652">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      You can add code inside a verbatim environment
    </p>
    <p>
      Do not forget to make the respective frame fragile
    </p>
    <p>
      by adding "fragile" as an attribute.
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1287036835801" ID="ID_124243487" MODIFIED="1287036841491" TEXT="Formatting">
<node CREATED="1287036842175" ID="ID_288529738" MODIFIED="1287036844962" TEXT="Formatting">
<node CREATED="1287036845543" ID="ID_96683410" MODIFIED="1287036853866" TEXT="Normal rule for formatting Latex apply"/>
<node CREATED="1287038854352" ID="ID_1183139625" MODIFIED="1287038975103" TEXT="math mode: $\int_{0}^{\infty}\frac{1}{x}dx$"/>
<node CREATED="1287038970683" ID="ID_296621743" MODIFIED="1287038988272" TEXT="\texttt{Source Code}"/>
<node CREATED="1287038992436" ID="ID_1762507918" MODIFIED="1287039005872" TEXT="Something \emph{important}"/>
</node>
</node>
<node CREATED="1287044484534" ID="ID_1379213645" MODIFIED="1287044492857" TEXT="Properties">
<node CREATED="1287047794905" ID="ID_334235911" MODIFIED="1287059175590" TEXT="Properties of frames" VSHIFT="-2">
<node CREATED="1287047803785" ID="ID_1408149644" MODIFIED="1287047809045" TEXT="attributes">
<node CREATED="1287047809649" ID="ID_1461679467" MODIFIED="1287047827981" TEXT="Rendered as attribute, e.g. fragile"/>
</node>
<node CREATED="1287047830322" ID="ID_1611556249" MODIFIED="1287047834069" TEXT="noitemize">
<node CREATED="1287047834626" ID="ID_756950904" MODIFIED="1287047913049">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Only render concatenated text of first-level children
    </p>
    <p>
      (All other children are ignored)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1287054157823" ID="ID_485298781" MODIFIED="1287054162386" TEXT="Title">
<node CREATED="1287044493558" ID="ID_262739024" MODIFIED="1287044579318">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      If you want to customize the template further,
    </p>
    <p>
      you may add properties above and use them
    </p>
    <p>
      in the template group without editing the
    </p>
    <p>
      source code
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1290340396305" ID="ID_844536444" MODIFIED="1290340440776" TEXT="Noitemize">
<attribute NAME="noitemize" VALUE="true"/>
<node CREATED="1290340403049" ID="ID_1426283520" MODIFIED="1290340404588" TEXT="Just"/>
<node CREATED="1290340405272" ID="ID_553590866" MODIFIED="1290340406324" TEXT="some"/>
<node CREATED="1290340408801" ID="ID_275202447" MODIFIED="1290340409891" TEXT="text"/>
<node CREATED="1290340410601" ID="ID_1717656629" MODIFIED="1290340420884" TEXT="without any list"/>
</node>
</node>
<node CREATED="1287058604644" ID="ID_630068147" MODIFIED="1287058607316" TEXT="Latex">
<node CREATED="1287058607895" ID="ID_551574987" MODIFIED="1287058609162" TEXT="Latex">
<node CREATED="1287058610759" ID="ID_1285433488" MODIFIED="1287066433851">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Note that underscores and apersands have to be quoted as "\_" "\&amp;"
    </p>
    <p>
      if you did not specify this in the relevant property
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1287039016773" ID="ID_558782765" MODIFIED="1287039059786" TEXT="End">
<node CREATED="1287039061399" ID="ID_1017860343" MODIFIED="1287047841455" TEXT="End">
<attribute NAME="noitemize" VALUE="true"/>
<node CREATED="1287039065126" ID="ID_1234885246" MODIFIED="1287044174487">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="margin-right: 0px; margin-top: 0px; text-indent: 0px; margin-left: 0px; margin-bottom: 0px">
      \begin{center}
    </p>
    <p style="margin-top: 0px; margin-right: 0px; text-indent: 0px; margin-left: 0px; margin-bottom: 0px">
      \begin{tikzpicture}
    </p>
    <p style="margin-top: 0px; margin-right: 0px; text-indent: 0px; margin-left: 0px; margin-bottom: 0px">
      \foreach \i in {0, 0.5,...,6} {
    </p>
    <p style="margin-top: 0px; margin-right: 0px; text-indent: 0px; margin-left: 0px; margin-bottom: 0px">
      \foreach \j in {0, 0.5,...,6} {
    </p>
    <p style="margin-top: 0px; margin-right: 0px; text-indent: 0px; margin-left: 0px; margin-bottom: 0px">
      \draw (0,\i) -- (10,\j);
    </p>
    <p style="margin-top: 0px; margin-right: 0px; text-indent: 0px; margin-left: 0px; margin-bottom: 0px">
      }
    </p>
    <p style="margin-top: 0px; margin-right: 0px; text-indent: 0px; margin-left: 0px; margin-bottom: 0px">
      }
    </p>
    <p style="margin-top: 0px; margin-right: 0px; text-indent: 0px; margin-left: 0px; margin-bottom: 0px">
      
    </p>
    <p style="margin-top: 0px; margin-right: 0px; text-indent: 0px; margin-left: 0px; margin-bottom: 0px">
      \end{tikzpicture}
    </p>
    <p style="margin-top: 0px; margin-right: 0px; text-indent: 0px; margin-left: 0px; margin-bottom: 0px">
      \par\end{center}
    </p>
  </body>
</html></richcontent>
<attribute NAME="linebreaks" VALUE="false"/>
</node>
</node>
</node>
</node>
</node>
</map>
