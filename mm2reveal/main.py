#!/usr/bin/env python3

from datetime import datetime, date
from lxml import etree
from pprint import pprint, pformat
import re

from lxml.etree import tostring, XPath

from jinja2 import Environment, FileSystemLoader

import logging
logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger(__file__)

body_text = XPath(r'html/body/*')
FIX_WS = re.compile(r'\s+')

DEFAULTS = {
    'place': 'Unterföhring',
    'date': str(date.today()),
    'author': 'somebody',
    'overview_name': 'Übersicht',
    'email': 'somebody@tngtech.com',
}
#liste = [1, 2, 'a', ('a','b')]
duple = (1, 2, 'a', ('a','b'))
dup1=(1,)
my_set = {1,2,2}
def fix_ws(input):
    s = FIX_WS.split(input); r = s[0]
    for s2 in s[1:]:
        if r[-1].isalpha() and s2[0].isalpha():
            r += " "
        r += s2
    return r


def text(node):
    """
    Parse one node.

    :param: node
    """
    content_list = [ c for c in node if c.tag == 'richcontent' ]
    if content_list:
        s = ''
        for c in content_list:
            res = body_text(c)
            log.info(res)
            for r in res:
                s += fix_ws(tostring(r, encoding=str))
    else:
        s = str(node.get('TEXT'))
    log.debug('text: '+s)
    return s.strip()


def render_page(data):
    env = Environment(loader=FileSystemLoader('templates'), trim_blocks=True)
    template = env.get_template('reveal.html')
    return template.render(**data)


def parse(node):
    title = text(node)
    log.info("Parsing node: "+title)
    attrs = {'title': title, 'SHOW': True}
    subs = []
    for sub in node:
        if sub.tag == 'attribute':
            key = sub.get('NAME')
            value = sub.get('VALUE')
            attrs[key]=value
            attrs['SHOW']=False
            continue
        if sub.tag in ('richcontent'):
            continue
        if sub.tag not in ('node', 'attribute'):
            log.warn("Ignoring '%s'"%sub.tag)
            continue
        b = parse(sub)
        subs.append(b)
    log.debug("Attrs:"+pformat(attrs))
    log.debug("Subs: "+','.join([ str(s) for s in subs])+'\n')
    return attrs, subs


def parse_config(node):
    data = {}
    for propnode in node:
        try:
            key, val = text(propnode).split('=')
            data[key.strip()] = val.strip()
        except:
            pass
    return data


def parse_root(base):
    data = {}
    for node in base:
        data['title'] = text(node)
        data['sections'] = []
        data['properties'] = {}
        for subnode in node:
            log.info('subnode: %s', subnode.get('TEXT'))
            if text(subnode) == 'properties':
                data['config'] = parse_config(subnode)
            elif text(subnode) == 'preamble':
                pass
            else:
                section = parse(subnode)
                data['sections'].append(section)
    return data


def emulate_latex_commands(node_attrs):
    text = node_attrs['title']
    for expr, repl in ( (r'\\texttt{(.*)}', r'<tt>\1</tt>'), (r'\\emph{(.*)}', r'<em>\1</em>'), ):
        text = re.sub(expr, repl, text)
    node_attrs['title'] = text


def postprocess(node, depth=0):
    emulate_latex_commands(node[0])
    if depth > 1 and node[1]:
        node[0]['class'] = 'no-burn'
    for n in node[1]:
        postprocess(n, depth+1)


def parse_tree(root, defaults = None):
    if defaults is None:
        defaults = DEFAULTS
    global data, section
    data = defaults
    data.update(parse_root(root.iterchildren('node')))
    for section in data['sections']:
        postprocess(section)
    return data


def main():
    global sys, input_file, tree, root, data, of
    import sys

    with open(sys.argv[1]) as input_file:
        tree = etree.parse(input_file)
        root = tree.getroot()
    data = parse_tree(root)
    log.warn(pformat(data))
    with open('index.html', 'w') as of:
        of.write(render_page(data))


if __name__ == '__main__':
    main()
